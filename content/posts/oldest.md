---
title: "Just checking this out ..."
description: "Created to test GitLab pages and static website generators such as Hugo"
date: "2017-03-02"
categories: 
    - "test"
    - "blah"
---

There should be some content here.

Also, forgot to add that the website is also interesting for testing GitLab CI/CD
tools.